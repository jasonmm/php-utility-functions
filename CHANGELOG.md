# Changelog

## 2019-01-13
### Abandoned

See the README.md file.

## [1.0.0] - 2017-08-15
### Added
- `array_map_set_key` and `array_map_keys` functions.


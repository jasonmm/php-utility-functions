<?php
declare(strict_types = 1);
namespace Jasonmm\UtilityFunctions\Tests;

use PHPUnit\Framework\TestCase;
use function Jasonmm\UtilityFunctions\array_map_set_key;
use function Jasonmm\UtilityFunctions\identity;

class FunctionsTest extends TestCase {
    public function test_array_map_set_key() {
        $testArray = [1, 2, 3, 4];
        $resultArray = \Jasonmm\UtilityFunctions\array_map_set_key(
            function ($item) {
                return $item;
            },
            $testArray,
            function ($item) {
                return $item + 1;
            });
        $this->assertTrue(array_keys($resultArray) === [2, 3, 4, 5],
            "Result array keys are incorrect: " .
            var_export(array_keys($resultArray), true));
        $this->assertTrue(array_values($resultArray) === [1, 2, 3, 4],
            "Result array values are incorrect: " .
            var_export(array_values($resultArray), true));
    }

    public function test_array_map_keys() {
        $testArray = ['one' => 1, 'two' => 2, 'three' => 3];
        $resultArray = \Jasonmm\UtilityFunctions\array_map_keys(
            function ($k, $v) {
                return 10 * $v;
            },
            $testArray
        );
        $this->assertTrue(array_keys($resultArray) === ['one', 'two', 'three'],
            "Result array keys are incorrect: " .
            var_export(array_keys($resultArray), true));
        $this->assertTrue(array_values($resultArray) === [10, 20, 30],
            "Result array values are incorrect: " .
            var_export(array_values($resultArray), true));
    }

    public function test_flatten_array() {
        $resultArray = \Jasonmm\UtilityFunctions\flatten_array(4);
        $this->assertTrue($resultArray === [4],
            "Result array is not [4]: " . var_export($resultArray, true));
        $this->assertCount(1, $resultArray,
            "Result array does not have only 1 item.");

        $testArray = [
            [1, 2, 3],
            [4, 5, 6]
        ];
        $resultArray = \Jasonmm\UtilityFunctions\flatten_array($testArray);
        $this->assertTrue($resultArray === [1, 2, 3, 4, 5, 6],
            "Result array is incorrect: " . var_export($resultArray, true));
    }

    public function test_identity() {
        $a = [
            ['id' => 1, 'branch' => 'master'],
            ['id' => 2, 'branch' => 'develop']
        ];
        $result = array_map_set_key(
            identity(),
            $a,
            function ($row) {
                return $row['id'];
            });

        $arrayKeys = array_keys($result);
        $arrayVals = array_values($result);
        $this->assertTrue($arrayKeys === [1, 2],
            'Array keys are not 1 and 2: ' . var_export($arrayKeys, true));
        $this->assertTrue($arrayVals === $a,
            'Array values are not the identity: ' . var_export($arrayVals,
                true));
    }
}

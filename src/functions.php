<?php
namespace Jasonmm\UtilityFunctions;

/**
 * Implementation of `array_map` that returns an associative array where each
 * key is the result of passing each input item to $kfn.
 *
 * Example:
 *     $a = [['one', 'two'], ['a', 'b'], ['uno', 'dos']];
 *     $assoc = array_map_set_key(
 *         function($item) { return $item[1]; },
 *         $a,
 *         function($item) { return $item[0]; });
 *     // $assoc === ['one' => 'two', 'a' => 'b', 'uno' => 'dos']
 *
 * @param callable $f   standard `array_map` callable
 * @param array $a      input array
 * @param callable $kfn a function that receives each value of $a and returns
 *                      the key that should be used for that value in the
 *                      returned array.
 *
 * @return array
 */
function array_map_set_key(callable $f, array $a, callable $kfn) : array {
    $ret = [];

    foreach( $a as $v ) {
        $ret[$kfn($v)] = $f($v);
    }

    return $ret;
}

/**
 * Implementation of `array_map` preserving the keys of the array.
 *
 * Example:
 *     $a = ['one' => 1];
 *     $result = array_map_keys(function($k, $v) { return $v+1; }, $a);
 *     // $result === ['one' => 2]
 *
 * @param callable $f a function receiving two arguments: the key and the value
 *                    of the current item in the array, the function should
 *                    return the item's new value.
 * @param array $a
 *
 * @return array
 */
function array_map_keys(callable $f, array $a) : array {
    $ret = [];

    foreach( $a as $k => $v ) {
        $ret[$k] = $f($k, $v);
    }

    return $ret;
}

/**
 * Takes an argument and returns a one-dimensional array.
 *
 * Used mostly for flattening multi-dimensional arrays into one dimension. If
 * $arg is not an array it is turned into an array and returned.
 *
 * @param mixed $arg
 *
 * @return array
 *
 * @see https://gist.github.com/zdenko/9a27b1f228fd8c8a59e3
 */
function flatten_array($arg) : array {
    return is_array($arg) ? array_reduce($arg, function ($c, $a) {
        return array_merge($c, flatten_array($a));
    }, []) : [$arg];
}

/**
 * Returns x. Helpful when another function requires a `callable`.
 *
 * Example:
 *     $a = [['id' => 1, 'branch' => 'master'],
 *           ['id' => 2, 'branch' => 'develop']];
 *     $result = array_map_set_key(
 *          identity(),
 *          $a,
 *          function($row) { return $row['id']; });
 *     // $result === [1 => ['id' => 1, 'branch' => 'master'],
 *     //              2 => ['id' => 2, 'branch' => 'develop']]
 *
 * @return callable
 */
function identity() {
    return function ($x) {
        return $x;
    };
}

